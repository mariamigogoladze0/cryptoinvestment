package com.example.cryptoinvestment.entity;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@Entity
@Table(name = "crypto_info")
public class CryptoInfo {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "date_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;

    @Column(name = "crypto_name")
    private String name;

    @Column(name = "price")
    private BigDecimal price;
}
