package com.example.cryptoinvestment.controller;

import com.example.cryptoinvestment.entity.CryptoInfo;
import com.example.cryptoinvestment.service.CryptoService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.AllArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

@RestController
@AllArgsConstructor
public class CryptoController {

    private CryptoService cryptoService;

    @GetMapping("/monthReport/{date}")
    @Operation(description = "Get oldest/newest/min/max for each crypto for the whole month")
    public Map<String, Map<String, CryptoInfo>> getMonthReport(@PathVariable @DateTimeFormat(pattern = "yyyy-MM") Date date) {
        return cryptoService.getMonthReport(date);
    }

    @GetMapping("/normalizedRangeReport")
    @Operation(description = "Return a descending sorted list of all the cryptos, comparing the normalized range")
    public Map<String, BigDecimal> getListOfCryptoInfosSortedByNormalizedRange() {
        return cryptoService.getReportSortedByNormalizedRange();
    }

    @GetMapping("/boundaryReport/{name}")
    @Operation(description = "Return the oldest/newest/min/max values for a requested crypto")
    public Map<String, CryptoInfo> getBoundaryReportByName(@PathVariable String name) {
        return cryptoService.getBoundaryReportByName(name);
    }

    @GetMapping("/highestNormalizedRangeReport/{date}")
    @Operation(description = "return the crypto with the highest normalized range for a specific day")
    public Map<String, BigDecimal> getHighestNormalizedRangeByDate(@PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") Date date) {
        return cryptoService.getHighestNormalizedRangeByDate(date);
    }

}
