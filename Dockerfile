FROM openjdk:17-jdk-slim
COPY target/crypto-investment.jar crypto-investment.jar
ENTRYPOINT ["java","-jar","/crypto-investment.jar"]